import java.util.*;

public class Game {
    private String Knight;
    private String Pawn;
    private String[] pawnAttackCells;
    private HashMap<Character, Integer> letterToDigit = new HashMap<>(8);

    public Game(String start, String pawn){
        this.Knight =start;
        this.Pawn = pawn;
        createDictionary();
        definePawnAttackCells();
    }

    public List<String> BFS(){
        List<String> visited = new ArrayList<>();
        HashMap<String, String> fathers = new HashMap<>();
        List<String> result = new ArrayList<>();
        ArrayDeque<String> queue = new ArrayDeque<>(getAvailableCells(Knight));
        for(String each : getAvailableCells(Knight)){
            fathers.put(each, Knight);
        }
        visited.add(Knight);
        while (queue.size() != 0) {
            String position = queue.pollFirst();
            if(position.equals(Pawn)) {
                result = getResult(position, fathers);
                break;
            }
            for(String eachPos : getAvailableCells(position)) {
                if(visited.contains(eachPos) || (pawnAttackCells != null &&
                  (eachPos.equals(pawnAttackCells[0]) || eachPos.equals(pawnAttackCells[1]))))
                	continue;
                if(!queue.contains(eachPos)) {
                    queue.add(eachPos);
                    fathers.put(eachPos, position);
                }
            }
            visited.add(position);
        }
        return result;
    }

    private List<String> getAvailableCells(String currentCell){
        List<String> availableCells = new ArrayList<>();
        int coordA = letterToDigit.get(currentCell.charAt(0));
        char coordAch = currentCell.charAt(0);
        int coordB = Character.getNumericValue(currentCell.charAt(1));
        if(coordB <= 6) {
            if (coordA <= 7)
                availableCells.add((char)(coordAch + 1)+""+(coordB + 2));
            if (coordA >= 2)
                availableCells.add((char)(coordAch - 1)+""+(coordB + 2));
        }
        if(coordA >= 3){
            if(coordB <= 7)
                availableCells.add((char)(coordAch - 2)+""+(coordB + 1));
            if(coordB >= 2)
                availableCells.add((char)(coordAch - 2)+""+(coordB - 1));
        }
        if(coordB >= 3) {
            if (coordA >= 2)
                availableCells.add((char)(coordAch - 1)+""+(coordB - 2));
            if (coordA <= 7)
                availableCells.add((char)(coordAch + 1)+""+(coordB - 2));
        }
        if(coordA <= 6){
            if(coordB >= 2)
                availableCells.add((char)(coordAch + 2)+""+(coordB - 1));
            if(coordB <= 7)
                availableCells.add((char)(coordAch+ 2)+""+(coordB + 1));
        }
        return availableCells;
    }

    private void createDictionary(){
        for(Character letter = 'a'; letter < 'i'; letter++)
            letterToDigit.put(letter, Character.getNumericValue(letter) - 9);

    }

    private void definePawnAttackCells(){
        pawnAttackCells = new String[2];
        if(Pawn.charAt(1) != '1'){
            if(Pawn.charAt(0) == 'a' || Pawn.charAt(0) == 'h') {
                if (Pawn.charAt(0) == 'h')
                    pawnAttackCells[0] = (char)(Pawn.charAt(0) - 1) + "" + (Character.getNumericValue(Pawn.charAt(1)) - 1);
                if (Pawn.charAt(1) == 'a')
                    pawnAttackCells[1] = (char)(Pawn.charAt(0) + 1) + "" + (Character.getNumericValue(Pawn.charAt(1)) - 1);
            }
            else {
                pawnAttackCells[0] = (char)(Pawn.charAt(0) - 1) + "" + (Character.getNumericValue(Pawn.charAt(1)) - 1);
                pawnAttackCells[1] = (char)(Pawn.charAt(0) + 1) + "" + (Character.getNumericValue(Pawn.charAt(1)) - 1);
            }
        }
        else pawnAttackCells = null;
    }

    private List<String> getResult(String end, HashMap<String, String> path){
        String current = end;
        List<String> result = new ArrayList<>();
        while(!current.equals(Knight)){
            result.add(current);
            current = path.get(current);
        }
        result.add(Knight);
        Collections.reverse(result);
        return result;
    }
}