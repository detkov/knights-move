Knight's Move.  
Solution of the knight's move task.  

In file ``in.txt`` you should write position of knight and in the next line position of pawn.  
After running the program in file ``out.txt`` you'll see route for kill from knight's position to pawn.