import java.io.*;
import java.util.List;

public class FileHandler {
    static Game readFile() throws IOException {
        String start=null, pawn=null;
        try(BufferedReader reader = new BufferedReader(new FileReader("in.txt")))
        {
            try {
                String s;
                for(int j=0; j < 2; j++){
                    s = reader.readLine();
                    if(j==0) start = s;
                    else pawn = s;
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                reader.close();
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return new Game(start, pawn);
    }
    static void writeFile(List<String> path) throws IOException{
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("out.txt"))){
            try{
                for(String each : path){
                    writer.write(each);
                    writer.newLine();
                }
            }
            catch (IOException e){
                e.printStackTrace();
            }
            finally {
                writer.flush();
                writer.close();
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}